package org.springframework.samples.petclinic.owner;

import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.samples.petclinic.vet.Specialty;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public interface PetTypeRepository extends Repository<PetType, Integer> {

	@Transactional
	PetType save(PetType entity);

	@Transactional
	PetType findById(Integer primaryKey);

	@Transactional
	void delete(PetType entity);

	@Transactional
	boolean existsById(Integer primaryKey);

	@Transactional(readOnly = true)
	ArrayList<PetType> findAll() throws DataAccessException;

	@Transactional(readOnly = true)
	Page<PetType> findAll(Pageable pageable) throws DataAccessException;

	@Transactional
	PetType findByName(String name);

}
