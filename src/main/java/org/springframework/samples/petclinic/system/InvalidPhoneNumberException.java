package org.springframework.samples.petclinic.system;

public class InvalidPhoneNumberException extends Exception {

	public InvalidPhoneNumberException(String message) {
		super(message);
	}

}
