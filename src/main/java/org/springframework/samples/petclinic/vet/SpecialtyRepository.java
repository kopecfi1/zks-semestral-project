package org.springframework.samples.petclinic.vet;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;

public interface SpecialtyRepository extends Repository<Specialty, Integer> {

	@Transactional
	Specialty save(Specialty entity);

	@Transactional
	Specialty findById(Integer primaryKey);

	@Transactional
	void delete(Specialty entity);

	@Transactional
	boolean existsById(Integer primaryKey);

	@Transactional(readOnly = true)
	ArrayList<Specialty> findAll() throws DataAccessException;

	@Transactional(readOnly = true)
	Page<Specialty> findAll(Pageable pageable) throws DataAccessException;

}
