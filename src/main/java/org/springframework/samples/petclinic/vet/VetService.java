package org.springframework.samples.petclinic.vet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
public class VetService {

	@Autowired
	protected VetRepository vets;

	@Autowired
	protected SpecialtyRepository specialities;

	@Transactional
	public void addVetWithSpecialty(Vet vet) {

		for (Specialty s : vet.getSpecialties()) {
			Integer id = s.getId();
			if (id != null && specialities.existsById(id))
				continue;
			specialities.save(s);
		}
		vets.save(vet);

	}

}
