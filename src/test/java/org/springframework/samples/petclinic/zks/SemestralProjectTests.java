package org.springframework.samples.petclinic.zks;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.samples.petclinic.model.Person;
import org.springframework.samples.petclinic.owner.*;
import org.springframework.samples.petclinic.service.EntityUtils;
import org.springframework.samples.petclinic.system.InvalidPhoneNumberException;
import org.springframework.samples.petclinic.vet.*;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.samples.petclinic.visit.VisitRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

@DataJpaTest(includeFilters = @ComponentScan.Filter(Service.class))
// Ensure that if the mysql profile is active we connect to the real database:
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class SemestralProjectTests {

	@Autowired
	protected OwnerRepository ownerRepository;

	@Autowired
	protected VetRepository vetRepository;

	@Autowired
	protected SpecialtyRepository specialtyRepository;

	@Autowired
	protected VetService vetService;

	@Autowired
	protected SpecialtyService specialtyService;

	@Autowired
	protected PetRepository petRepository;

	@Autowired
	protected PetTypeRepository petTypeRepository;

	@Autowired
	protected PetService petService;

	@Autowired
	protected VisitRepository visitRepository;

	@Test
	void shouldAddNewVet() {
		Vet vet = new Vet();
		vet.setFirstName("Tomáš");
		vet.setLastName("Sedlár");
		vetRepository.save(vet);

		Integer id = vet.getId();
		Vet fetchedVet = vetRepository.findById(id);
		assertThat(fetchedVet).isNotNull();
		assertThat(fetchedVet.getFirstName()).isEqualTo("Tomáš");
		assertThat(fetchedVet.getLastName()).isEqualTo("Sedlár");
		assertThat(fetchedVet.getSpecialties()).isEmpty();
		assertThat(fetchedVet.getNrOfSpecialties()).isEqualTo(0);
	}

	@Test
	void shouldAddVetWithSpecialities() {
		Vet vet = new Vet();
		vet.setFirstName("Marek");
		vet.setLastName("Nacházel");

		Specialty specialty = specialtyRepository.findById(1);
		assertThat(specialty).isNotNull();
		assertThat(specialty.getName()).isEqualTo("radiology");
		vet.addSpecialty(specialty);
		vetRepository.save(vet);

		Integer id = vet.getId();
		Vet fetchedVet = vetRepository.findById(id);

		assertThat(fetchedVet.getFirstName()).isEqualTo("Marek");
		assertThat(fetchedVet.getLastName()).isEqualTo("Nacházel");
		assertThat(fetchedVet.getSpecialties()).isNotEmpty();
		assertThat(fetchedVet.getNrOfSpecialties()).isEqualTo(1);
		assertThat(fetchedVet.getSpecialties().size()).isEqualTo(1);

	}

	@Test
	void shouldThrowExceptionWhenVetNameIsTooLong() {
		Vet vet = new Vet();
		vet.setFirstName("Marek");
		vet.setLastName(
				"HodněDlouhéPříjmeníHodněDlouhéPříjmeníHodněDlouhéPříjmeníHodněDlouhéPříjmeníHodněDlouhéPříjmeníHodněDlouhéPříjmení");
		Assertions.assertThrows(DataIntegrityViolationException.class, () -> vetRepository.save(vet));

		Vet vet1 = new Vet();
		vet1.setFirstName("JmenoJmenoJmenoJmenoJmenoJmenoJ");
		vet1.setLastName("KOP");
		Assertions.assertThrows(DataIntegrityViolationException.class, () -> vetRepository.save(vet1));

		Vet vet2 = new Vet();
		vet2.setFirstName("JmenoJmenoJmenoJmenoJmenoJmenoJ");
		vet2.setLastName("PrijmeniPrijmeniPrijmeniPrijmen");
		Assertions.assertThrows(DataIntegrityViolationException.class, () -> vetRepository.save(vet2));

		Vet vet3 = new Vet();
		vet3.setFirstName("Filip");
		vet3.setLastName("Kopecký");
		Assertions.assertDoesNotThrow(() -> vetRepository.save(vet3));

	}

	@Test
	void shouldAddNewVetWithNewSpecialty() {
		Specialty specialty = new Specialty();
		specialty.setName("test");

		Vet vet = new Vet();
		vet.setFirstName("Marek");
		vet.setLastName("Nacházel");
		vet.addSpecialty(specialty);

		vetService.addVetWithSpecialty(vet);

		Integer id = vet.getId();
		Vet fetchedVet = vetRepository.findById(id);
		assertThat(fetchedVet).isNotNull();
		assertThat(fetchedVet.getNrOfSpecialties()).isEqualTo(1);

		ArrayList<Specialty> specialtyCollection = specialtyRepository.findAll();
		assertThat(specialtyCollection.contains(specialty)).isTrue();
		assertThat(specialtyCollection.get(specialtyCollection.size() - 1).getName()).isEqualTo("test");
		assertThat(specialtyRepository.findById(vet.getSpecialties().get(0).getId())).isNotNull();

	}

	@Test
	void shouldRemoveSpecialtyFromVet() {
		Vet vet = vetRepository.findById(2);
		assertThat(vet).isNotNull();
		assertThat(vet.getNrOfSpecialties()).isEqualTo(1);
		Specialty specialtyToBeRemoved = vet.getSpecialties().get(0);
		vet.removeSpecialty(specialtyToBeRemoved);
		vetRepository.save(vet);

		Vet fetchedVet = vetRepository.findById(2);
		assertThat(fetchedVet).isNotNull();
		assertThat(fetchedVet.getNrOfSpecialties()).isEqualTo(0);
		assertThat(fetchedVet.getSpecialties()).isEmpty();

		Specialty specialty = specialtyRepository.findById(specialtyToBeRemoved.getId());
		assertThat(specialty).isNotNull();

	}

	@Test
	void shouldAddNewSetOfSpecialtiesToVet() {
		Set<Specialty> set = new HashSet<Specialty>();
		for (int i = 0; i < 4; i++) {
			Specialty s = new Specialty();
			s.setName("Test" + i);
			set.add(s);
		}

		Vet vetWithNoSpecials = vetRepository.findById(7);
		assertThat(vetWithNoSpecials.getSpecialties()).isEmpty();
		assertThat(vetWithNoSpecials.getNrOfSpecialties()).isEqualTo(0);

		vetWithNoSpecials.setSpecialties(set);
		vetService.addVetWithSpecialty(vetWithNoSpecials);

		Vet fetchedVet = vetRepository.findById(vetWithNoSpecials.getId());
		assertThat(fetchedVet.getNrOfSpecialties()).isEqualTo(4);
		assertThat(fetchedVet.getSpecialties()).hasSize(4);
		for (int i = 0; i < 4; i++) {
			Specialty specialty = specialtyRepository.findById(fetchedVet.getSpecialties().get(i).getId());
			assertThat(specialty.getName()).isEqualTo("Test" + i);
		}

	}

	@Test
	void shouldRemoveVet() {
		Vet vetToBeRemoved = vetRepository.findById(7);
		assertThat(vetToBeRemoved).isNotNull();
		Collection<Vet> originalSet = vetRepository.findAll();
		Integer numberOfVets = originalSet.size();

		assertThat(originalSet).contains(vetToBeRemoved);

		vetRepository.delete(vetToBeRemoved);

		Vet nonExistingVet = vetRepository.findById(7);
		assertThat(nonExistingVet).isNull();

		Collection<Vet> allVets = vetRepository.findAll();
		assertThat(allVets).hasSize(numberOfVets - 1);
		assertThat(allVets).doesNotContain(vetToBeRemoved);

	}

	@Test
	void shouldGetVetsByTheirFirstName() {
		List<Vet> vets_list = vetRepository.findByFirstName("Henry");
		assertThat(vets_list).hasSize(2);
		assertThat(vets_list.get(0).getLastName()).isEqualTo("Stevens");
		assertThat(vets_list.get(1).getLastName()).isEqualTo("Smith");

		List<Vet> empty_list = vetRepository.findByFirstName("NonExistingName");
		assertThat(empty_list).isNotNull();
		assertThat(empty_list).hasSize(0);
		assertThat(empty_list.size()).isEqualTo(0);

	}

	@Test
	void shouldNotAddNullSpecialtyToVet() {
		ArrayList<Specialty> og_specials = specialtyRepository.findAll();

		Specialty specialtyNull = null;
		Assertions.assertThrows(InvalidDataAccessApiUsageException.class,
				() -> specialtyRepository.save(specialtyNull));

		Vet vetWithNoSpecials = vetRepository.findById(7);
		assertThat(vetWithNoSpecials.getSpecialties()).isEmpty();
		assertThat(vetWithNoSpecials.getNrOfSpecialties()).isEqualTo(0);
		vetWithNoSpecials.addSpecialty(specialtyNull);
		vetService.addVetWithSpecialty(vetWithNoSpecials);

		ArrayList<Specialty> n_specials = specialtyRepository.findAll();

		Vet fetchedVet = vetRepository.findById(7);
		assertThat(fetchedVet.getSpecialties()).isEmpty();
		assertThat(fetchedVet.getNrOfSpecialties()).isEqualTo(0);
		assertThat(og_specials).isEqualTo(n_specials);

	}

	@Test
	void shouldFindVetsByTheirSpecialties() {
		Specialty radiology = specialtyRepository.findById(1);
		Specialty surgery = specialtyRepository.findById(2);
		Specialty dentistry = specialtyRepository.findById(3);

		List<Vet> radiologist = vetRepository.findBySpecialtiesContaining(radiology);
		List<Vet> surgeons = vetRepository.findBySpecialtiesContaining(surgery);
		List<Vet> dentists = vetRepository.findBySpecialtiesContaining(dentistry);

		// We are sure that the data is present from the start
		assertThat(radiologist).isNotEmpty();
		assertThat(surgeons).isNotEmpty();
		assertThat(dentists).isNotEmpty();

		for (Vet v : radiologist) {
			assertThat(v.getSpecialties()).contains(radiology);
		}
		for (Vet v : surgeons) {
			assertThat(v.getSpecialties()).contains(surgery);
		}
		for (Vet v : dentists) {
			assertThat(v.getSpecialties()).contains(dentistry);
		}

	}

	@Test
	void shouldFindVetsByTheirSpecialtyName() {
		List<Vet> radiologist = vetRepository.findBySpecialty("radiology");
		List<Vet> surgeons = vetRepository.findBySpecialty("surgery");
		List<Vet> dentists = vetRepository.findBySpecialty("dentistry");
		assertThat(radiologist).isNotNull();
		assertThat(radiologist).isNotEmpty();
		assertThat(surgeons).isNotNull();
		assertThat(surgeons).isNotEmpty();
		assertThat(dentists).isNotNull();
		assertThat(dentists).isNotEmpty();

		for (Vet v : radiologist) {
			boolean passes = false;
			for (Specialty specialty : v.getSpecialties()) {
				if (specialty.getName().equals("radiology")) {
					passes = true;
				}
			}
			assertThat(passes).isTrue();
		}
		for (Vet v : surgeons) {
			boolean passes = false;
			for (Specialty specialty : v.getSpecialties()) {
				if (specialty.getName().equals("surgery")) {
					passes = true;
				}
			}
			assertThat(passes).isTrue();
		}
		for (Vet v : dentists) {
			boolean passes = false;
			for (Specialty specialty : v.getSpecialties()) {
				if (specialty.getName().equals("dentistry")) {
					passes = true;
				}
			}
			assertThat(passes).isTrue();
		}
	}

	@Test
	void shouldDeleteSpecialty() {

		Specialty radiology = specialtyRepository.findById(1);
		List<Vet> radiologist = vetRepository.findBySpecialty("radiology");
		assertThat(radiology).isNotNull();
		assertThat(radiology.getName()).isEqualTo("radiology");
		assertThat(radiologist).isNotEmpty();

		specialtyService.deleteSpecialty(radiology);

		List<Vet> radiologist2 = vetRepository.findBySpecialty("radiology");
		assertThat(radiologist2).isEmpty();
		Specialty radiology2 = specialtyRepository.findById(1);
		assertThat(radiology2).isNull();

		ArrayList<Specialty> allSpecialties = specialtyRepository.findAll();
		for (Specialty s : allSpecialties) {
			assertThat(s.getName()).isNotEqualTo("radiology");
		}

		Collection<Vet> allVets = vetRepository.findAll();
		for (Vet v : allVets) {
			boolean passes = true;
			for (Specialty s : v.getSpecialties()) {
				if (s.getName().equals("radiology"))
					passes = false;
			}
			assertThat(passes).isTrue();
		}

	}

	@Test
	void shouldGetAllPetTypes() {
		List<PetType> petTypes = petRepository.findPetTypes();
		assertThat(petTypes).isNotEmpty();
		assertThat(petTypes).hasSize(6);
		assertThat(EntityUtils.getById(petTypes, PetType.class, 1).getName()).isEqualTo("cat");
		assertThat(EntityUtils.getById(petTypes, PetType.class, 2).getName()).isEqualTo("dog");
		assertThat(EntityUtils.getById(petTypes, PetType.class, 3).getName()).isEqualTo("lizard");
		assertThat(EntityUtils.getById(petTypes, PetType.class, 4).getName()).isEqualTo("snake");
		assertThat(EntityUtils.getById(petTypes, PetType.class, 5).getName()).isEqualTo("bird");
		assertThat(EntityUtils.getById(petTypes, PetType.class, 6).getName()).isEqualTo("hamster");
		// Checks that fetched types are really stored in the database
		assertThat(petTypeRepository.findById(1).getName()).isEqualTo("cat");
		assertThat(petTypeRepository.findById(2).getName()).isEqualTo("dog");
		assertThat(petTypeRepository.findById(3).getName()).isEqualTo("lizard");
		assertThat(petTypeRepository.findById(4).getName()).isEqualTo("snake");
		assertThat(petTypeRepository.findById(5).getName()).isEqualTo("bird");
		assertThat(petTypeRepository.findById(6).getName()).isEqualTo("hamster");
	}

	@Test
	void shouldAddPetType() {

		List<PetType> petTypes = petRepository.findPetTypes();
		Integer og_size = petTypes.size();
		PetType horseType = new PetType();
		horseType.setName("horse");
		petService.addPetType(horseType);
		List<PetType> modifiedPetTypes = petRepository.findPetTypes();
		// Checks if the added petType is present
		assertThat(modifiedPetTypes).isNotNull();
		assertThat(modifiedPetTypes).isNotEmpty();
		assertThat(modifiedPetTypes.size()).isEqualTo(og_size + 1);
		assertThat(modifiedPetTypes).contains(horseType);
		// Trying to add already existing pet type
		PetType catType = new PetType();
		catType.setName("cat");
		petService.addPetType(catType);
		List<PetType> modifiedPetTypes2 = petRepository.findPetTypes();
		// Checks that the size is the same as before insertion
		assertThat(modifiedPetTypes2).isNotNull();
		assertThat(modifiedPetTypes2).hasSize(modifiedPetTypes.size());

	}

	@Test
	void shouldFindPetTypeByName() {
		PetType catType = petTypeRepository.findByName("cat");
		assertThat(catType).isNotNull();
		assertThat(catType.getName()).isEqualTo("cat");
		assertThat(catType.toString()).isEqualTo("cat");
		PetType dogType = petTypeRepository.findByName("dog");
		assertThat(dogType).isNotNull();
		assertThat(dogType.getName()).isEqualTo("dog");
		assertThat(dogType.toString()).isEqualTo("dog");
		PetType nonExistingType = petTypeRepository.findByName("non_existing_type");
		assertThat(nonExistingType).isNull();

	}

	@Test
	void shouldFindPetsByTypeName() {
		List<Pet> pets = petRepository.findByTypeName("cat");
		assertThat(pets).isNotEmpty();
		for (Pet p : pets) {
			assertThat(p.getType()).isNotNull();
			assertThat(p.getOwner()).isNotNull();
			assertThat(p.getBirthDate()).isNotNull();
			assertThat(p.getType().getName()).isEqualTo("cat");
		}
	}

	@Test
	void shouldDeletePetType() {
		Collection<Pet> allPets = petRepository.findAll();
		Integer og_size = allPets.size();
		List<Pet> pets = petRepository.findByTypeName("cat");
		assertThat(pets).isNotEmpty();
		List<PetType> petTypes = petTypeRepository.findAll();
		PetType cats = petTypeRepository.findByName("cat");
		Integer catNumber = pets.size();
		petTypeRepository.delete(cats);
		List<PetType> petTypes2 = petTypeRepository.findAll();
		assertThat(petTypes.size()).isEqualTo(petTypes2.size() + 1);
		List<Pet> pets2 = petRepository.findByTypeName("cat");
		assertThat(pets2).isEmpty();
		Collection<Pet> allPets2 = petRepository.findAll();
		assertThat(allPets2).hasSize(og_size - catNumber);

	}

	@Test
	void shouldAddNewPhoneNumberToOwner() {
		Owner owner = new Owner();
		Assertions.assertThrows(InvalidPhoneNumberException.class, () -> owner.setTelephone(null));
		Assertions.assertThrows(InvalidPhoneNumberException.class, () -> owner.setTelephone("phoneNumber"));
		Assertions.assertThrows(InvalidPhoneNumberException.class, () -> owner.setTelephone("132"));
		Assertions.assertThrows(InvalidPhoneNumberException.class, () -> owner.setTelephone("45kk55"));
		Assertions.assertDoesNotThrow(() -> owner.setTelephone("1234567890"));
		try {
			owner.setFirstName("Test");
			owner.setLastName("Testovic");
			owner.setTelephone("1234567890");
			owner.setAddress("address");
			owner.setCity("city");
			// owner.getPets();
			ownerRepository.save(owner);
			assertThat(ownerRepository.findById(owner.getId())).isNotNull();
		}
		catch (Exception e) {
			Assertions.fail();
		}

	}

	@Test
	void shouldAddVisit() {
		Collection<Visit> og_pet_visits = visitRepository.findByPetId(1);
		Collection<Visit> og_visits = visitRepository.findAll();
		Visit visit = new Visit();
		visit.setDate(LocalDate.now());
		visit.setDescription("Test visit");
		visit.setPetId(1);
		visitRepository.save(visit);
		Collection<Visit> modified_visits = visitRepository.findAll();
		Collection<Visit> modified_pet_visits = visitRepository.findByPetId(1);

		assertThat(og_visits).hasSize(modified_visits.size() - 1);
		assertThat(og_pet_visits).hasSize(modified_pet_visits.size() - 1);

		Visit lastVisit = visitRepository.findById(visit.getId());
		assertThat(lastVisit).isNotNull();
		assertThat(lastVisit.getDate()).isEqualTo(visit.getDate());
		assertThat(lastVisit.getPetId()).isEqualTo(visit.getPetId());
		assertThat(lastVisit.getDescription()).isEqualTo(visit.getDescription());

	}

	@Test
	void shouldRemoveVisit() {
		Collection<Visit> og_visits = visitRepository.findAll();

		Visit visit = visitRepository.findById(1);
		assertThat(visit).isNotNull();
		Collection<Visit> pet_visits = visitRepository.findByPetId(visit.getPetId());
		visitRepository.delete(visit);

		Visit fetchedVisit = visitRepository.findById(1);
		assertThat(fetchedVisit).isNull();
		Collection<Visit> pet_visits2 = visitRepository.findByPetId(visit.getPetId());
		Collection<Visit> modified_visits = visitRepository.findAll();
		assertThat(og_visits).hasSize(modified_visits.size() + 1);
		assertThat(pet_visits).hasSize(pet_visits2.size() + 1);

	}

	@ParameterizedTest
	@CsvFileSource(resources = "/dataset.csv", numLinesToSkip = 1)
	public void vetConstructor(String address, String city, String firstname, String lastname, String phone,
			String expected) {
		try {
			Owner owner = new Owner(address, city, phone, firstname, lastname);
			assertThat(expected).isEqualTo("valid");
			assertThat(address).isEqualTo(owner.getAddress());
			assertThat(city).isEqualTo(owner.getCity());
			assertThat(firstname).isEqualTo(owner.getFirstName());
			assertThat(lastname).isEqualTo(owner.getLastName());
			assertThat(phone).isEqualTo(owner.getTelephone());
		}
		catch (IllegalArgumentException e) {
			assertThat(expected).isEqualTo("invalid");
		}

	}

	@ParameterizedTest
	@MethodSource("pets")
	public void constructor_pet_test(LocalDate birthDate, PetType petType, Owner owner, String expected) {
		try {
			Pet pet = new Pet(birthDate, petType, owner);
			assertThat(pet.getBirthDate()).isEqualTo(birthDate);
			assertThat(pet.getType()).isNotNull();
			assertThat(pet.getOwner()).isNotNull();
			assertThat(expected).isEqualTo("valid");
		}
		catch (IllegalArgumentException e) {
			assertThat(expected).isEqualTo("invalid");
		}
	}

	private static Stream<Arguments> pets() {
		return Stream.of(arguments(LocalDate.of(2000, 1, 1), new PetType(), new Owner(), "valid"),
				arguments(LocalDate.of(2000, 1, 1), null, new Owner(), "invalid"),
				arguments(LocalDate.of(2000, 1, 1), new PetType(), null, "invalid"),
				arguments(LocalDate.of(2000, 1, 1), null, null, "invalid"),
				arguments(LocalDate.of(1999, 12, 31), new PetType(), new Owner(), "invalid"),
				arguments(LocalDate.of(2000, 1, 1), new PetType(), null, "invalid"),
				arguments(LocalDate.of(2000, 1, 1), null, null, "invalid"),
				arguments(LocalDate.of(1999, 12, 31), null, new Owner(), "invalid"),
				arguments(LocalDate.of(1999, 12, 31), new PetType(), null, "invalid"),
				arguments(LocalDate.of(1999, 12, 31), null, null, "invalid"));
	}

	@ParameterizedTest
	@CsvFileSource(resources = "/person-output.csv", numLinesToSkip = 1)
	public void vetConstructor(String firstname, String lastname, String expected) {
		try {
			Person person = new Person(firstname, lastname);
			assertThat(person.getFirstName()).isEqualTo(firstname);
			assertThat(person.getLastName()).isEqualTo(lastname);
			assertThat(expected).isEqualTo("valid");
		}
		catch (IllegalArgumentException e) {
			assertThat(expected).isEqualTo("invalid");
		}

	}

	@ParameterizedTest
	@MethodSource("vets")
	public void constructor_vet_test(String firstname, String lastname, Set<Specialty> specialty, String expected) {
		try {
			Vet vet = new Vet(firstname, lastname, specialty);
			assertThat(vet.getSpecialties()).isNotNull();
			assertThat(vet.getFirstName()).isEqualTo(firstname);
			assertThat(vet.getLastName()).isEqualTo(lastname);
			assertThat(expected).isEqualTo("valid");
		}
		catch (IllegalArgumentException e) {
			assertThat(expected).isEqualTo("invalid");
		}
	}

	private static Stream<Arguments> vets() {
		Specialty specialty = new Specialty();
		specialty.setName("testingSpecialty");

		Set<Specialty> set = new HashSet<Specialty>();
		set.add(specialty);

		return Stream.of(arguments("FIL", "KOP", set, "valid"),
				arguments("FIL", "PrijmeniPrijmeniPrijmeniPrijme", set, "valid"),
				arguments("JmenoJmenoJmenoJmenoJmenoJmeno", "KOP", set, "valid"),
				arguments("JmenoJmenoJmenoJmenoJmenoJmeno", "PrijmeniPrijmeniPrijmeniPrijme", set, "valid"),
				arguments("FIL", null, set, "invalid"), arguments("FIL", "KO", set, "invalid"),
				arguments("FIL", "PrijmeniPrijmeniPrijmeniPrijmen", set, "invalid"),
				arguments("JmenoJmenoJmenoJmenoJmenoJmeno", null, set, "invalid"),
				arguments("JmenoJmenoJmenoJmenoJmenoJmeno", "KO", set, "invalid"),
				arguments("JmenoJmenoJmenoJmenoJmenoJmeno", "PrijmeniPrijmeniPrijmeniPrijmen", set, "invalid"),
				arguments(null, "KOP", set, "invalid"),
				arguments(null, "PrijmeniPrijmeniPrijmeniPrijme", set, "invalid"),
				arguments("FI", "KOP", set, "invalid"),
				arguments("FI", "PrijmeniPrijmeniPrijmeniPrijme", set, "invalid"),
				arguments("JmenoJmenoJmenoJmenoJmenoJmenoJ", "KOP", set, "invalid"),
				arguments("JmenoJmenoJmenoJmenoJmenoJmenoJ", "PrijmeniPrijmeniPrijmeniPrijme", set, "invalid"),
				arguments("FIL", "KOP", null, "invalid"),
				arguments("JmenoJmenoJmenoJmenoJmenoJmeno", "PrijmeniPrijmeniPrijmeniPrijme", null, "invalid"));
	}

}
